package main

import (
	"os"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/report/v4"
)

func TestReadDependencyScanningReport(t *testing.T) {

	// Test with valid file
	r, err := readDependencyScanningReport("testfiles/sample-gl-dependency-scanning-report.json")
	if err != nil {
		t.Errorf("Unexpected error: %v", err)
	}
	if r.Version.String() != "15.0.7" {
		t.Error("Expected report, got nil")
	}

	// Test with non-existing file
	r, err = readDependencyScanningReport("does_not_exist.json")
	if err == nil {
		t.Error("Expected error, got nil")
	}

	// Test with invalid JSON
	r, err = readDependencyScanningReport("testfiles/invalid.json")
	if err == nil {
		t.Error("Expected error, got nil")
	}
}

func TestIsOnDenyList_OnList(t *testing.T) {
	identifiers := []report.Identifier{{Type: report.IdentifierTypeCVE, Value: "CVE-2022-1234"}}
	os.Setenv("CVE_DENY_LIST", "CVE-2022-1234")

	result := isOnDenyList(identifiers)

	if !result {
		t.Error("Expected CVE to be on deny list")
	}
}

func TestIsOnDenyList_NotOnList(t *testing.T) {
	identifiers := []report.Identifier{{Type: report.IdentifierTypeCVE, Value: "CVE-2022-1234"}}
	os.Setenv("CVE_DENY_LIST", "CVE-2022-5678")

	result := isOnDenyList(identifiers)

	if result {
		t.Error("Did not expect CVE to be on deny list")
	}
}

func TestIsOnDenyList_MultipleCVEs(t *testing.T) {
	identifiers := []report.Identifier{
		{Type: report.IdentifierTypeCVE, Value: "CVE-2022-1234"},
		{Type: report.IdentifierTypeCVE, Value: "CVE-2022-5678"},
	}
	os.Setenv("CVE_DENY_LIST", "CVE-2022-1234,CVE-2022-5678")

	result := isOnDenyList(identifiers)

	if !result {
		t.Error("Expected both CVEs to be on deny list")
	}
}

func TestGetFileNames_NoArgs(t *testing.T) {
	os.Args = []string{"test"}
	read, write := getFileNames()
	if read != "gl-dependency-scanning-report.json" {
		t.Errorf("unexpected read filename: %s", read)
	}
	if write != "gl-dependency-scanning-report.json" {
		t.Errorf("unexpected write filename: %s", write)
	}
}

func TestGetFileNames_OneArg(t *testing.T) {
	os.Args = []string{"test", "input.json"}
	read, write := getFileNames()
	if read != "input.json" {
		t.Errorf("unexpected read filename: %s", read)
	}
	if write != "gl-dependency-scanning-report.json" {
		t.Errorf("unexpected write filename: %s", write)
	}
}

func TestGetFileNames_TwoArgs(t *testing.T) {
	os.Args = []string{"test", "input.json", "output.json"}
	read, write := getFileNames()
	if read != "input.json" {
		t.Errorf("unexpected read filename: %s", read)
	}
	if write != "output.json" {
		t.Errorf("unexpected write filename: %s", write)
	}
}

// When CVE_DENY_LIST isnt set, do not re-rate any vulnerabilities
func TestShouldntReRateVulnerabilities(t *testing.T) {

	os.Unsetenv("CVE_DENY_LIST")

	r := report.Report{
		Vulnerabilities: []report.Vulnerability{
			{
				Severity: report.SeverityLevelCritical,
				Identifiers: []report.Identifier{
					{Type: report.IdentifierTypeCVE, Value: "CVE-2023-456"},
				},
			},
		},
	}

	deRateCriticalVulnsIfNotOnDenylist(&r)

	if r.Vulnerabilities[0].Severity != report.SeverityLevelCritical {
		t.Errorf("Expected severity to be re-rated to %s, got %s",
			report.SeverityLevelCritical,
			r.Vulnerabilities[0].Severity)
	}
}

// When CVE_DENY_LIST is empty but exists, re-rate any vulnerabilities
func TestShouldReRateVulnerabilities(t *testing.T) {

	os.Setenv("CVE_DENY_LIST", "")

	r := report.Report{
		Vulnerabilities: []report.Vulnerability{
			{
				Severity: report.SeverityLevelCritical,
				Identifiers: []report.Identifier{
					{Type: report.IdentifierTypeCVE, Value: "CVE-2023-456"},
				},
			},
		},
	}

	deRateCriticalVulnsIfNotOnDenylist(&r)

	if r.Vulnerabilities[0].Severity != report.SeverityLevelHigh {
		t.Errorf("Expected severity to be re-rated to %s, got %s",
			report.SeverityLevelHigh,
			r.Vulnerabilities[0].Severity)
	}
}

func TestReRateVulnerabilities(t *testing.T) {

	os.Setenv("CVE_DENY_LIST", "CVE-456")

	r := report.Report{
		Vulnerabilities: []report.Vulnerability{
			{
				Severity: report.SeverityLevelCritical,
				Identifiers: []report.Identifier{
					{Type: report.IdentifierTypeCVE, Value: "CVE-2023-1234"},
				},
			},
			{
				Severity: report.SeverityLevelCritical,
				Identifiers: []report.Identifier{
					{Type: report.IdentifierTypeCVE, Value: "CVE-456"},
				},
			},
		},
	}

	deRateCriticalVulnsIfNotOnDenylist(&r)

	if r.Vulnerabilities[0].Severity != report.SeverityLevelHigh {
		t.Errorf("Expected severity to be re-rated to %s, got %s",
			report.SeverityLevelHigh,
			r.Vulnerabilities[0].Severity)
	}
	if r.Vulnerabilities[1].Severity != report.SeverityLevelCritical {
		t.Errorf("Expected severity to be re-rated to %s, got %s",
			report.SeverityLevelCritical,
			r.Vulnerabilities[1].Severity)
	}
}

func TestShouldReRateVulnerability(t *testing.T) {

	testCases := []struct {
		vuln     report.Vulnerability
		expected bool
	}{
		{
			vuln: report.Vulnerability{
				Severity: report.SeverityLevelCritical,
			},
			expected: true,
		},
		{
			vuln: report.Vulnerability{
				Severity: report.SeverityLevelHigh,
			},
			expected: false,
		},
	}

	for _, tc := range testCases {
		actual := shouldReRateVulnerability(tc.vuln)
		if actual != tc.expected {
			t.Errorf("Expected shouldReRateVulnerability to return %t, got %t",
				tc.expected, actual)
		}
	}
}

func TestDenyListMatchRerateVulnerability(t *testing.T) {

	os.Setenv("CVE_DENY_LIST", "CVE-456")

	testCases := []struct {
		vuln     report.Vulnerability
		expected bool
	}{
		{
			vuln: report.Vulnerability{
				Severity: report.SeverityLevelCritical,
				Identifiers: []report.Identifier{
					{Type: report.IdentifierTypeCVE, Value: "CVE-123"},
				},
			},
			expected: true,
		},
		{
			vuln: report.Vulnerability{
				Severity: report.SeverityLevelCritical,
				Identifiers: []report.Identifier{
					{Type: report.IdentifierTypeCVE, Value: "CVE-456"},
				},
			},
			expected: false,
		},
	}

	for _, tc := range testCases {
		actual := shouldReRateVulnerability(tc.vuln)
		if actual != tc.expected {
			t.Errorf("Expected shouldReRateVulnerability to return %t, got %t",
				tc.expected, actual)
		}
	}
}

func TestCalculateCVSSBaseScoreFromVector(t *testing.T) {

	// Test CVSS v2.0 vector
	vector := "AV:N/AC:L/Au:N/C:P/I:P/A:P"
	expectedScore := 7.5

	score, err := calculateCVSSBaseScoreFromVector(vector)
	if err != nil {
		t.Errorf("Unexpected error: %v", err)
	}
	if score != expectedScore {
		t.Errorf("Expected score: %f, got: %f", expectedScore, score)
	}

	// Test CVSS v3.0 vector
	vector = "CVSS:3.0/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H"
	expectedScore = 9.8

	score, err = calculateCVSSBaseScoreFromVector(vector)
	if err != nil {
		t.Errorf("Unexpected error: %v", err)
	}
	if score != expectedScore {
		t.Errorf("Expected score: %f, got: %f", expectedScore, score)
	}

	// Test another CVSS v3.0 vector https://nvd.nist.gov/vuln/detail/CVE-2014-10064
	vector = "CVSS:3.0/AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:N/A:H"
	expectedScore = 7.5

	score, err = calculateCVSSBaseScoreFromVector(vector)
	if err != nil {
		t.Errorf("Unexpected error: %v", err)
	}
	if score != expectedScore {
		t.Errorf("Expected score: %f, got: %f", expectedScore, score)
	}

	// Test a CVSS v3.1 vector https://nvd.nist.gov/vuln/detail/CVE-2021-44906
	vector = "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H"
	expectedScore = 9.8

	score, err = calculateCVSSBaseScoreFromVector(vector)
	if err != nil {
		t.Errorf("Unexpected error: %v", err)
	}
	if score != expectedScore {
		t.Errorf("Expected score: %f, got: %f", expectedScore, score)
	}

	// Test to ensure calculateCVSSBaseScoreFromVector returns error 1 when provided an invalid vector
	vector = "invalid"

}

func TestCalculateCVSSBaseScore_InvalidVector(t *testing.T) {
	vector := "invalid"

	score, err := calculateCVSSBaseScoreFromVector(vector)
	if err == nil {
		t.Error("Expected error for invalid vector but got none")
	}
	if score != 0 {
		t.Errorf("Expected 0 score, got: %f", score)
	}
}

func TestCalculateCVSSBaseScore_CVSS4(t *testing.T) {
	vector := "CVSS:4.0/AV:N/AC:L/AT:P/PR:N/UI:N/VC:H/VI:L/VA:L/SC:N/SI:N/SA:N"

	score, err := calculateCVSSBaseScoreFromVector(vector)
	if err == nil {
		t.Error("Expected error for CVSS4.0 vector, but got none")
	}
	if score != 0 {
		t.Errorf("Expected 0 score, got: %f", score)
	}
}

func TestDeRateBasedOnCVSSBaseScore(t *testing.T) {

	t.Run("does not re-rate critical vulns if base score is above threshold", func(t *testing.T) {
		r := &report.Report{
			Vulnerabilities: []report.Vulnerability{
				{
					Severity: report.SeverityLevelCritical,
					CVSSRatings: []report.CVSSRating{
						{
							Vendor: "NVD",
							// calculates to 9.8 https://nvd.nist.gov/vuln-metrics/cvss/v3-calculator?vector=AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H&version=3.1
							Vector: "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H",
						},
					},
				},
			},
		}
		os.Setenv("MININUM_CVSS_SCORE_FOR_CRITICAL", "9.0")

		deRateBasedOnCVSSBaseScore(r)

		if r.Vulnerabilities[0].Severity != report.SeverityLevelCritical {
			t.Error("Expected vulnerability to be rated as critical")
		}
	})

	t.Run("rates critical vulns as high if base score is below threshold", func(t *testing.T) {
		r := &report.Report{
			Vulnerabilities: []report.Vulnerability{
				{
					Severity: report.SeverityLevelCritical,
					CVSSRatings: []report.CVSSRating{
						{
							Vendor: "NVD",
							// calculates to 8.7 https://nvd.nist.gov/vuln-metrics/cvss/v3-calculator?vector=AV:N/AC:L/PR:L/UI:R/S:C/C:L/I:L/A:N&version=3.1
							Vector: "CVSS:3.1/AV:N/AC:L/PR:L/UI:R/S:C/C:L/I:L/A:N",
						},
					},
				},
			},
		}
		os.Setenv("MININUM_CVSS_SCORE_FOR_CRITICAL", "9.0")

		deRateBasedOnCVSSBaseScore(r)

		if r.Vulnerabilities[0].Severity != report.SeverityLevelHigh {
			t.Error("Expected vulnerability to be rated as high")
		}
	})

	t.Run("does not re-rate vulns if $MININUM_CVSS_SCORE_FOR_CRITICAL is not set", func(t *testing.T) {
		r := &report.Report{
			Vulnerabilities: []report.Vulnerability{
				{
					Severity: report.SeverityLevelCritical,
					CVSSRatings: []report.CVSSRating{
						{
							Vendor: "NVD",
							// calculates to 9.8 https://nvd.nist.gov/vuln-metrics/cvss/v3-calculator?vector=AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H&version=3.1
							Vector: "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H",
						},
					},
				},
			},
		}

		os.Unsetenv("MININUM_CVSS_SCORE_FOR_CRITICAL")

		deRateBasedOnCVSSBaseScore(r)

		if r.Vulnerabilities[0].Severity != report.SeverityLevelCritical {
			t.Error("Expected no re-rating")
		}
	})
}
