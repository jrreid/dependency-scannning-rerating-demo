package main

import (
	"encoding/json"
	"errors"
	"io"
	"os"
	"strconv"
	"strings"

	"github.com/joho/godotenv"
	gocvss20 "github.com/pandatix/go-cvss/20"
	gocvss30 "github.com/pandatix/go-cvss/30"
	gocvss31 "github.com/pandatix/go-cvss/31"
	log "github.com/sirupsen/logrus"
	"gitlab.com/gitlab-org/security-products/analyzers/report/v4"
)

func main() {

	// Reads a .env file if present in working directory
	// handy for local invocation during testing.
	godotenv.Load()

	// Set read & write filenames for json reports from CLI arguments
	readFilename, writeFilename := getFileNames()

	// Read the json report file and return as a report.Report object
	r, err := readDependencyScanningReport(readFilename)
	if err != nil {
		log.Fatal(err)
	}

	// if $CVE_DENY_LIST exists (can be set empty), then
	// loop through vulnerabilities; if the severity is Critical,
	// and the CVE is *NOT* on the deny list, then change the severity to High
	// otherwise, if CVE *IS* on the deny list, leave as Critical
	// ---
	// if $CVE_DENY_LIST is *not set*, don't do any re-rating
	deRateCriticalVulnsIfNotOnDenylist(&r)

	// if $MININUM_CVSS_SCORE_FOR_CRITICAL is set to a floating point value, then
	// loop through vulns, calculate the CVSS Base Score from the provided NVD CVSS vector
	// and if the base score is less than $MININUM_CVSS_SCORE_FOR_CRITICAL,
	// change severity to high
	deRateBasedOnCVSSBaseScore(&r)

	// save jsonFile to (overwriting the original file)
	// TODO: Refactor into its own function
	newJson, err := json.MarshalIndent(r, "", "  ")
	if err != nil {
		log.Fatal(err)
	}
	err = os.WriteFile(writeFilename, newJson, 0644)
	if err != nil {
		log.Fatal(err)
	}
}

// if $MININUM_CVSS_SCORE_FOR_CRITICAL is set to a floating point value, then
// loop through vulns, calculate the CVSS Base Score from the provided NVD CVSS vector
// and if the base score of a critical vuln is less than $MININUM_CVSS_SCORE_FOR_CRITICAL,
// then change the vuln's severity to high
func deRateBasedOnCVSSBaseScore(r *report.Report) {

	// pull decimal/float value from $MININUM_CVSS_SCORE_FOR_CRITICAL env variable
	minCVSSScoreString, isSet := os.LookupEnv(("MININUM_CVSS_SCORE_FOR_CRITICAL"))

	if isSet && (len(minCVSSScoreString) > 0) { // if no error, and $MININUM_CVSS_SCORE_FOR_CRITICAL is set to something, loop through vulns
		minimumCVSSScore, err := strconv.ParseFloat(minCVSSScoreString, 64)
		if err != nil {
			log.Error(err)
		} else {
			log.Infof("Minimum CVSS Score for Critical Vulns: %0.01f", minimumCVSSScore)
			log.Info("------------------------------------------------------------------")
			for i := 0; i < len(r.Vulnerabilities); i++ {

				var alreadyReratedVuln bool = false
				vuln := &r.Vulnerabilities[i]

				log.Info("Processing Vulnerability ID: " + vuln.ID())

				// Only re-rate Critical vulns
				if vuln.Severity == report.SeverityLevelCritical {
					for i := range vuln.CVSSRatings {
						// Provided a 3.x CVSS Vector from NVD, calculate the base score
						if (vuln.CVSSRatings[i].Vendor == "NVD") && (strings.HasPrefix(vuln.CVSSRatings[i].Vector, "CVSS:3")) {
							baseScore, err := calculateCVSSBaseScoreFromVector(vuln.CVSSRatings[i].Vector)
							if err != nil {
								log.Error(err)
							} else if !alreadyReratedVuln {
								// only re-rate each vuln once (we'll loop through here for all provided CVSS Ratings)
								alreadyReratedVuln = true
								if baseScore < minimumCVSSScore {
									reRateVulnerability(vuln, report.SeverityLevelHigh, "Calculated base score of "+strconv.FormatFloat(baseScore, 'f', 2, 64)+" is less than $MININUM_CVSS_SCORE_FOR_CRITICAL ("+minCVSSScoreString+"), re-rating to High")
								} else {
									log.Info("Critical vuln's calculated CVSS base score of " + strconv.FormatFloat(baseScore, 'f', 2, 64) + " exceeds $MININUM_CVSS_SCORE_FOR_CRITICAL (" + minCVSSScoreString + "),; not re-rating")
								}
							}
						} else if !alreadyReratedVuln {
							log.Warn("No NVD-provided CVSS3.x vector found; cannot re-rate")
						}
					}
				} else {
					log.Info("Vuln is not Critical; not re-rating")
				}
				log.Info("------------------------------------------------------------------")
			}
		}
	} else {
		log.Warn("$MININUM_CVSS_SCORE_FOR_CRITICAL env variable not set, not re-rating vulns based on this criteria")
	}
}

// if `CVE_DENY_LIST` is set as an env variable, re-rate critical vulns to high,
// unless they're present on the comma-delimited 'denylist', in which case, leave them critical
func deRateCriticalVulnsIfNotOnDenylist(r *report.Report) {

	_, isSet := os.LookupEnv(("CVE_DENY_LIST"))
	if isSet {
		for i := 0; i < len(r.Vulnerabilities); i++ {
			vuln := &r.Vulnerabilities[i]

			if shouldReRateVulnerability(*vuln) {
				reRateVulnerability(vuln, report.SeverityLevelHigh, "not on CVE_DENY_LIST")
			}
		}
	} else {
		log.Warn("$CVE_DENY_LIST env variable not set, not re-rating vulns based on this criteria")
	}

}

func reRateVulnerability(vuln *report.Vulnerability, severity report.SeverityLevel, criteria string) {
	originalDescription := vuln.Description
	vuln.Description = "[RE-RATED] [Original Severity: " + vuln.Severity.String() + "] " + "[Criteria: " + criteria + "] " + originalDescription
	vuln.Severity = severity
	log.Warnf("Re-rated vuln %s severity to %s!",
		vuln.ID(), vuln.Severity)
}

func shouldReRateVulnerability(vuln report.Vulnerability) bool {
	return vuln.Severity == report.SeverityLevelCritical &&
		!isOnDenyList(vuln.Identifiers)
}

// Check to see if a provided array of vulnerability identifiers
// has a CVE identifier that is on the deny list, return true if so
// and false if not
func isOnDenyList(identifiers []report.Identifier) bool {
	var DenyList []string = strings.Split(os.Getenv("CVE_DENY_LIST"), ",")
	log.Infof("Deny list from $CVE_DENY_LIST: %s", DenyList)
	for i := 0; i < len(identifiers); i++ {
		if identifiers[i].Type == report.IdentifierTypeCVE {
			log.Infof("Comparing %s to deny list", identifiers[i].Value)
			for _, value := range DenyList {
				if identifiers[i].Value == value {
					log.Warnf(identifiers[i].Value + " is on the deny list— no re-rating of severity will occur.")
					return true
				}
			}
		}
	}
	return false
}

// Provided a filename, read and return the gl-dependency-scanning-report.json
// then unmarshal into an report.Report object and return it
func readDependencyScanningReport(filename string) (report.Report, error) {

	var r report.Report

	// open our jsonFile
	jsonFile, err := os.Open(filename)
	if err != nil {
		log.Error(err)
		return r, err
	}

	// read our opened jsonFile as a byte array.
	byteValue, _ := io.ReadAll(jsonFile)
	// defer the closing of our jsonFile so that we can parse it later on
	defer jsonFile.Close()

	// unmarshal our byteArray
	err = json.Unmarshal(byteValue, &r)
	if err != nil {
		log.Error(err)
		return r, err
	}

	return r, err
}

// Set read/write filenames from CLI arguments
// CLI syntax: $ go run re-rate-vulns.go report-to-be-read.json report-to-be-written.json
func getFileNames() (string, string) {

	const defaultFilename = "gl-dependency-scanning-report.json"

	readFilename := defaultFilename
	writeFilename := defaultFilename

	if len(os.Args) > 1 {
		readFilename = os.Args[1]

		if len(os.Args) > 2 {
			writeFilename = os.Args[2]
		} else {
			log.Info("Only one argument provided, using default 'gl-dependency-scanning-report.json' for output file")
		}
	} else {
		log.Info("No filename cli arguments provided, using default 'gl-dependency-scanning-report.json'")
	}

	return readFilename, writeFilename
}

func calculateCVSSBaseScoreFromVector(vector string) (float64, error) {

	var baseScore float64
	var err error

	log.Info("CVSS Vector: " + vector)

	switch {
	default: // Should be CVSS v2.0 or is invalid
		log.Infof("CVSS v%.01f", 2.0)
		cvss, err := gocvss20.ParseVector(vector)
		if err != nil {
			log.Error(err)
			return baseScore, err
		}
		baseScore = cvss.BaseScore()
	case strings.HasPrefix(vector, "CVSS:3.0"):
		log.Infof("CVSS v%.01f", 3.0)
		cvss, err := gocvss30.ParseVector(vector)
		if err != nil {
			log.Error(err)
			return baseScore, err
		}
		baseScore = cvss.BaseScore()
	case strings.HasPrefix(vector, "CVSS:3.1"):
		log.Infof("CVSS v%.01f", 3.1)
		cvss, err := gocvss31.ParseVector(vector)
		if err != nil {
			log.Error(err)
			return baseScore, err
		}
		baseScore = cvss.BaseScore()
	case strings.HasPrefix(vector, "CVSS:4.0"):
		//cvss, err := gocvss40.ParseVector(vector)
		// if err != nil {
		// 	log.Fatal(err)
		// }
		log.Error("CVSS 4.0 not supported")
		err = errors.New("CVSS 4.0 not supported")
		return baseScore, err
	}

	log.Infof("CVSS Base Score: %0.01f", baseScore)

	return baseScore, err
}
